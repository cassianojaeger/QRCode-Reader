var url = require('url');
var http = require('http');
var express = require('express');
var server = express();
var indexRouter = require('./routes/routes.js');

const hostname = '127.0.0.1';
const port = 8001;

server.use("/", indexRouter);
server.use(express.static('static_scripts'))

//listen for request on port 3000, and as a callback function have the port listened on logged
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
