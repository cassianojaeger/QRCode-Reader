var express = require('express');
var router = express.Router();
var mainController = require("../controllers/mainController")

// viewed at http://localhost:8080
router.get('/', mainController.index);

module.exports = router;
