let opts = {
  continuous: true,
  video: document.getElementById('preview'),
  mirror: true,
  captureImage: false,
  backgroundScan: true,
  refractoryPeriod: 5000,
  scanPeriod: 1
};
let scanner = new Instascan.Scanner(opts);
scanner.addListener('scan', function (content) {
  // URL for SCP
  url = "http://ptsv2.com/t/3xj1v-1532303288/post";
  data = { qrCodeScan: content };
  $.post(url , data)
    .done(function( data ) {
      alert( "Data Loaded: " + data )
    })
    .always(function( data ) {
       alert( "data loader: " + data )
    });
});
Instascan.Camera.getCameras().then(function (cameras) {
  if (cameras.length > 0) {
    scanner.start(cameras[0]);
  } else {
    console.error('No cameras found.');
  }
}).catch(function (e) {
  console.error(e);
});
